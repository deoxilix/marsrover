require "spec_helper"

describe "South" do
  before :each do
    @south = South.new
  end

  it "There should be a valid instance of South" do
    expect(@south).to be_an_instance_of South
  end

  it "should return an instance of East, on turning 90 degrees to the right" do
    expect(@south.right).to be_an_instance_of West
  end

  it "should return an instance West, on turning 90 degrees to the left" do
    expect(@south.left).to be_an_instance_of East
  end
end
