require "spec_helper"

describe "West" do
  before :each do
    @west = West.new
  end

  it "There should be a valid instance of West" do
    expect(@west).to be_an_instance_of West
  end

  it "should return an instance of East, on turning 90 degrees to the right" do
    expect(@west.right).to be_an_instance_of North
  end

  it "should return an instance West, on turning 90 degrees to the left" do
    expect(@west.left).to be_an_instance_of South
  end
end
