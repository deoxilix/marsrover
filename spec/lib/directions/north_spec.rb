require "spec_helper"

describe "North" do
  before :each do
    @north = North.new
  end

  it "There should be a valid instance of North" do
    expect(@north).to be_an_instance_of North
  end

  it "should return an instance of East, on turning 90 degrees to the right" do
    expect(@north.right).to be_an_instance_of East
  end

  it "should return an instance West, on turning 90 degrees to the left" do
    expect(@north.left).to be_an_instance_of West
  end
end
