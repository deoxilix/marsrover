require "spec_helper"

describe "East" do
  before :each do
    @east = East.new
  end

  it "There should be a valid instance of East" do
    expect(@east).to be_an_instance_of East
  end

  it "should return an instance of East, on turning 90 degrees to the right" do
    expect(@east.right).to be_an_instance_of South
  end

  it "should return an instance West, on turning 90 degrees to the left" do
    expect(@east.left).to be_an_instance_of North
  end
end
