require "spec_helper"

describe Rover do
  before :each do
    @rover = Rover.new(0, 5, "N")
    @rover_prime = Rover.new(0, 5, "S")
  end

  it "There should be a valid Rover instance" do
    expect(@rover).to be_an_instance_of Rover
  end

  it "rover should know its x coordinate" do
    expect(@rover.x_coordinate).to eq(0)
  end

  it "rover should know its y coordinate" do
    expect(@rover.y_coordinate).to eq(5)
  end

  it "rover should know its facing direction" do
    expect(@rover.facing_direction).to be_an_instance_of North
  end

  it "rover should move one grid in its facing direction, hence its corresponding coordinate to change by 1" do
    expect { @rover.move }.to change { @rover.y_coordinate }.from(5).to(6)
  end

  it "rover should rotate 90 degrees to right on instruction - 'R'" do
    @rover.rotate('R')
    expect(@rover.facing_direction).to be_an_instance_of East
  end

  it "rover should rotate 90 degrees to left on instruction - 'L'" do
    @rover.rotate('L')
    expect(@rover.facing_direction).to be_an_instance_of West
  end

  it "rover_prime should rotate 90 degrees to right on instruction - 'R'" do
    @rover_prime.rotate('R')
    expect(@rover_prime.facing_direction).to be_an_instance_of West
  end

  it "rover_prime should rotate 90 degrees to left on instruction - 'L'" do
    @rover_prime.rotate('L')
    expect(@rover_prime.facing_direction).to be_an_instance_of East
  end

  it "rover should be able to return its current state" do
    expect(@rover.state).to eq("0 5 N")
  end

  it "rover should be able to follow_sequence_of_instructions and return its consequent state " do
    expect { @rover.follow_instructions("LRMLLM") }.to change {@rover.state}.from("0 5 N").to("0 5 S")
  end

  it "rover should know which plateau it's on" do
    @plateau = Plateau.new(10,20)
    @rover.plateau = @plateau
    expect(@rover.plateau).to be_an_instance_of Plateau
  end

  it "rover should raise_exception for any instruction that makes it go off its Plateau" do
    @rover = Rover.new(10, 20, "E")
    @plateau = Plateau.new(10, 20)
    @rover.plateau = @plateau
    expect{ @rover.move }.to raise_exception(Exception);
  end
end
