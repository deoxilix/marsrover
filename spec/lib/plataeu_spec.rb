require "spec_helper"

describe Plateau do
  before :each do
    @plateau = Plateau.new(5, 10)
  end

  it "There should be a valid Plateau instance" do
    expect(@plateau).to be_an_instance_of Plateau
  end

  it "plateau should know its length" do
    expect(@plateau.length).to eq(5)
  end

  it "plateau should know its breadth" do
    expect(@plateau.breadth).to eq(10)
  end
end
