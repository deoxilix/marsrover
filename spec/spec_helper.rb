require 'rspec'

require_relative '../lib/rover'
require_relative '../lib/plateau'
require_relative '../lib/compass'
require_relative '../lib/directions/north'
require_relative '../lib/directions/south'
require_relative '../lib/directions/east'
require_relative '../lib/directions/west'

# require_relative '../dist/coordinate'

RSpec.configure do |config|
  config.color = true
  config.tty = true
  config.formatter = :documentation
end
