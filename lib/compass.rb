module Compass
  module Directions
    N = "N"
    S = "S"
    E = "E"
    W = "W"
  end

  VALID_ROTATIONS = [
      "L",
      "R"
  ]

  def parse_direction(direction)
    case direction
    when "N"
      North.new
    when "E"
      East.new
    when "W"
      West.new
    when "S"
      South.new
    end
  end
end
