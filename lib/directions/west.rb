class West
  attr_accessor :direction

  def initialize
    @direction = Compass::Directions::W
  end

  def right
    North.new
  end

  def left
    South.new
  end
end
