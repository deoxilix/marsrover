class East
  attr_accessor :direction

  def initialize
    @direction = Compass::Directions::N
  end

  def right
    South.new
  end

  def left
    North.new
  end
end
