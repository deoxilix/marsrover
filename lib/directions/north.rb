class North
  attr_accessor :direction

  def initialize
    @direction = Compass::Directions::N
  end

  def right
    East.new
  end

  def left
    West.new
  end 
end
