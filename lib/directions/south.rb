class South
  attr_accessor :direction

  def initialize
    @direction = Compass::Directions::S
  end

  def right
    West.new
  end

  def left
    East.new
  end
end
