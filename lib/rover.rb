require_relative "./compass"
# require 'pry'

class Rover
  include Compass
  attr_accessor :x_coordinate, :y_coordinate, :facing_direction, :plateau

  def initialize(x_coordinate, y_coordinate, facing_direction)
    @x_coordinate = x_coordinate.to_i
    @y_coordinate = y_coordinate.to_i
    @facing_direction = parse_direction(facing_direction)
    # binding.pry
  end

  def move
    case @facing_direction
      when North
        @y_coordinate += 1
      when South
        @y_coordinate -= 1
      when East
        @x_coordinate += 1
      when West
        @x_coordinate -= 1
      else
        Exception.new("Direction not recognised!");
    end
    raise Exception.new("Rover toppled over!") unless (within_plateau? || within_plateau?.nil?)
  end

  def rotate(instruction)
    if instruction == "R"
      @facing_direction = @facing_direction.right
    elsif instruction == "L"
      @facing_direction = @facing_direction.left
    end
  end

  def follow_instructions(instructions)
    instructions.split("").each do |instruction|
      case
      when Compass::VALID_ROTATIONS.include?(instruction)
        self.rotate(instruction)
      when instruction == "M"
        self.move
      end
    end
  end

  def state
    "#{self.x_coordinate} #{self.y_coordinate} #{self.facing_direction.direction}"
  end

  private

  def within_plateau?
    unless self.plateau.nil?
      self.x_coordinate.between?(0, self.plateau.length) && self.y_coordinate.between?(0, self.plateau.breadth)
    end
  end

end
