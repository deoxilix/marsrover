%w{
  lib/rover
  lib/plateau
  lib/directions/north
  lib/directions/south
  lib/directions/east
  lib/directions/west
}.each{|kalss| require_relative kalss }
require 'pry'

class MissionControl

  def self.transmit
    input = File.new("input.txt", "r")
    output = File.new("output.txt", "w+")
    rover_meta = []

    plateau = build_plateau(input.gets.chomp)
    while(line = input.gets)
      rover_meta << line.chomp unless line.chomp.empty?
    end

    counter = 0
    while counter < rover_meta.count
      rover = build_rover(rover_meta[counter])
      rover.plateau = plateau unless plateau.nil?

      rover.follow_instructions(rover_meta[counter+1])
      output.write(rover.state + "\n")
      counter += 2
    end
  end

  private

  def self.build_plateau (plateau_cordinate)
    unless plateau_cordinate.empty?
      plateau_cordinate = plateau_cordinate.split(" ").map(&:to_i)
      Plateau.new(plateau_cordinate[0], plateau_cordinate[1])
    end
  end

  def self.build_rover (rover_cordinate)
    unless rover_cordinate.empty?
      rover_cordinate = rover_cordinate.split(" ")
      Rover.new(rover_cordinate[0], rover_cordinate[1], rover_cordinate[2])
    end
  end
end

MissionControl.transmit
